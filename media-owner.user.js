// ==UserScript==
// @name Media owner
// @description Who owns the media that feed information
// @icon https://www.youtube.com/favicon.ico
// @match https://www.youtube.com/results*
// @updateURL http://localhost:8080/media-owner.user.js
// ==/UserScript==

/*
* FOREWORD
* ========
* In 2011, Google and other advertising corporations started to get interests
* in who they called partners and later embelished as "creators".
* https://trends.google.com/trends/explore?date=all&geo=US&q=creator
*
* They shifted afterwards (2015) their advertising content from blockable ads
* to promoted content from media, and advertized/sponsored content.
*
* Those flagship advertisers are known as "influencers".
* https://trends.google.com/trends/explore?date=all&q=influencer
*
* It draws the hierarchy
*
*      [influencers]
*              ===[influence]===>
*                      [influenceable target]
*
* The marketed word spread and the marketing/newspeak legion had to euphemize.
* Newspeak term is "curator". It's a guardian of assets.
* Namely the guardian of an advertising clientele asset.
*
*
* Previously, in feodal times or even more ancient times.
* Rulers and vassals owned lands, people and other economical powers.
* Religions influenced the pleb, curated and grew their powers.
*
* In today's Market Society(TM).
* Rulers/owners are the corporations and the states.
* Media are relevant warrants of their powers.
*
* Owning a media direction guarantees to enforce down an ideologic agenda.
* For instance, Dassault group manufactures weapons for France and its allies.
* Its agenda doesn't consist of promoting weapons to Le Figaro readers.
* Instead, it follows an ideology that promotes its production
* and geopolitical moves that benefit its productions sustainability.
* When comes the times of elections, the ideology will help electing leaders
* that will nourish those interests, in a mutually beneficial way.
*
* Nevertheless, most media journalists have a margin of freedom
* amongst these agenda, which mostly depends on the organization topology:
* a strict centralized hierarchy masters better over a decentralized one.
*
* Media are gate keepers of powers, but may serve the power of the people.
*
*
* PURPOSE
* =======
* Therefore, feeds are not to be taken under a conspiracy angle.
* Knowing who owns the media
* helps building critical thinking when digesting information feeds.
*/

(function disown() {
    let owners = {
        "Xavier Niel": {
            fortune: "3.9B$",
            wikifr: "https://fr.wikipedia.org/wiki/Xavier_Niel"
        },
        "Mathieu Pigasse": {
            fortune: "22M€",
            wikifr: "https://fr.wikipedia.org/wiki/Matthieu_Pigasse"
        },
        "Pierre Bergé": {
            fortune: "180M€",
            wikifr: "https://fr.wikipedia.org/wiki/Pierre_Berg%C3%A9"

        },
        "Alain Weill": {
            fortune: "240M€",
          wikifr: "https://fr.wikipedia.org/wiki/Alain_Weill"
        },
        "Martin Bouygues": {
            fortune: "4.4B$",
            wikifr: "https://fr.wikipedia.org/wiki/Martin_Bouygues"
        },
        "Bernard Arnault": {
            fortune: "66,9B€",
            wikifr: "https://fr.wikipedia.org/wiki/Martin_Bouygues"
        },
        "Daniel Kretinsky": {
            fortune: "",
            wikifr: "https://fr.wikipedia.org/wiki/Daniel_K%C5%99et%C3%ADnsk%C3%BD"
        },
        "Daniel Kretinsky": {
            fortune: "$7.9",
            wikifr: "https://fr.wikipedia.org/wiki/Patrick_Drahi"
        },
        "Vincent Bolloré": {
            fortune: "6B€",
            wikifr: "https://fr.wikipedia.org/wiki/Vincent_Bolloré"
        },
        "FrenchGov/APE": {
            fortune: "509,5B€",
            wikifr: "https://fr.wikipedia.org/wiki/Agence_des_participations_de_l%27%C3%89tat"
        },
        "Thomas Rabe": {
            fortune: "1~5M€",
            wikifr: "https://fr.wikipedia.org/wiki/Thomas_Rabe_(manager)"
        },
        "Famille Dassault": {
            fortune: "23,8B€",
            wikifr: "https://fr.wikipedia.org/wiki/Marcel_Dassault"
        },
        "Arnaud Lagardère": {
            fortune: "220M€",
            wikifr: "https://fr.wikipedia.org/wiki/Arnaud_Lagard%C3%A8re"
        },
        "Naguib Sawiris": {
            fortune: "$5.5B",
            wikifr: "https://fr.wikipedia.org/wiki/Naguib_Sawiris",
            wikien: "https://en.wikipedia.org/wiki/Naguib_Sawiris"
        },
        "Brian L. Roberts": {
            fortune: "$1.7B",
            wikifr: "https://fr.wikipedia.org/wiki/Brian_L._Roberts",
            wikien: "https://en.wikipedia.org/wiki/Brian_L._Roberts"
        }
    }

    let corporations = {
        /* === EUROPE === */
            /* === France === */
            // private
            "Groupe Le Monde": {
                _wikifr: "https://fr.wikipedia.org/wiki/Groupe_Le_Monde",
                _wikien: "",
                _capital: "392M€",
                _owners: ["Xavier Niel", "Mathieu Pigasse", "Pierre Bergé"]
            },
            "Altice": {
                _wikifr: "https://fr.wikipedia.org/wiki/Altice_France",
                _wikien: "https://en.wikipedia.org/wiki/Altice_(company)",
                _capital: "-15B€, 10B€/y",
                _owners: ["Alain Weill"]
            },
            "Bouygues": {
                _wikifr: "https://fr.wikipedia.org/wiki/Bouygues",
                _wikien: "https://en.wikipedia.org/wiki/Bouygues",
                _capital: "35B€",
                _owners: ["Martin Bouygues"]
            },
            "LVMH": {
                _wikifr: "https://fr.wikipedia.org/wiki/LVMH_-_Mo%C3%ABt_Hennessy_Louis_Vuitton",
                _wikien: "",
                _capital: "156B€",
                _owners: ["Bernard Arnault"]
            },
            "Bolloré": {
                _wikifr: "https://fr.wikipedia.org/wiki/Bolloré",
                _wikien: "",
                _capital: "11.38B€",
                _owners: ["Vincent Bolloré"]
            },
            "Le Média": {
                _wikifr: "https://fr.wikipedia.org/wiki/Le_M%C3%A9dia",
                _wikien: "",
                _capital: "1.8M€",
                _owners: []
            },
            "Mediapart": {
                _wikifr: "https://fr.wikipedia.org/wiki/mediapart",
                _wikien: "https://en.wikipedia.org/wiki/mediapart",
                _capital: "16,2M€",
                _owners: []
            },
            "Fiducial": {
                _wikifr: "https://fr.wikipedia.org/wiki/Fiducial",
                _wikien: "",
                _capital: "1.6B€",
                _owners: ["Christian Latouche"]
            },
            "Bertelsmann": {
                _wikifr: "https://fr.wikipedia.org/wiki/Bertelsmann",
                _wikien: "",
                _capital: "16.9B€",
                _owners: ["Thomas Rabe"]
            },
            "Groupe Dassault": {
                _wikifr: "https://fr.wikipedia.org/wiki/Groupe_industriel_Marcel_Dassault",
                _wikien: "https://en.wikipedia.org/wiki/Dassault_Group",
                _capital: "9.5B€",
                _owners: ["Famille Dassault"]
            },
            "Lagardère": {
                _wikifr: "https://fr.wikipedia.org/wiki/Lagard%C3%A8re_(entreprise)",
                _wikien: "",
                _capital: "7.2B€",
                _owners: ["Arnaud Lagardère"]
            },
            "Mediawan": {
                _wikifr: "https://fr.wikipedia.org/wiki/Mediawan",
                _wikien: "https://en.wikipedia.org/wiki/Mediawan",
                _capital: "$372M",
                _owners: ["Xavier Niel", "Mathieu Pigasse"]
            },

            // STATE
            "Radio France": {
                _wikifr: "https://fr.wikipedia.org/wiki/Radio_France",
                _wikien: "",
                _capital: "656M€",
                _owners: ["FrenchGov/APE"]
            },
            "France Médias Monde": {
                _wikifr: "https://fr.wikipedia.org/wiki/France_M%C3%A9dias_Monde",
                _wikien: "https://en.wikipedia.org/wiki/France_M%C3%A9dias_Monde",
                _capital: "272M€",
                _owners: ["FrenchGov/APE"]
            },
            "France Télévisions": {
                _wikifr: "https://fr.wikipedia.org/wiki/France_T%C3%A9l%C3%A9visions",
                _wikien: "",
                _capital: "3B€",
                _owners: ["FrenchGov/APE"]
            },
            "INA": {
                _wikifr: "https://fr.wikipedia.org/wiki/INA",
                _wikien: "",
                _capital: "37.9M€",
                _owners: []
            },
            "La Chaîne parlementaire": {
                _wikifr: "https://fr.wikipedia.org/wiki/France_T%C3%A9l%C3%A9visions",
                _wikien: "",
                _capital: "18M€",
                _owners: ["Assemblée nationale"]
            },
            "AFP": {
                _wikifr: "https://fr.wikipedia.org/wiki/Agence_France-Presse",
                _wikien: "https://en.wikipedia.org/wiki/Agence_France-Presse",
                _capital: "$110M",
                _owners: [""]
            },
            "Arte": {
                _wikifr: "https://fr.wikipedia.org/wiki/Arte",
                _wikien: "https://en.wikipedia.org/wiki/Arte",
                _capital: "$485M",
                _owners: ["GEGI/Arte France/Deutschland"]
            },

            /* --- Misc --- */
            "Alj Agency": {
                _wikifr: "https://fr.wikipedia.org/wiki/Madmoizelle.com",
                _wikien: "",
                _capital: "1.2M€",
                _owners: ["Fabrice Florent"]
            }

            /* === UK === */
            "BBC": {
                _wikifr: "https://fr.wikipedia.org/wiki/British_Broadcasting_Corporation",
                _wikien: "https://en.wikipedia.org/wiki/BBC",
                _capital: "£4.889B",
                _owners: ["UK Gov"]
            },
            "Channel 4": {
                _wikifr: "https://fr.wikipedia.org/wiki/Channel_Four_Television_Corporation",
                _wikien: "https://en.wikipedia.org/wiki/Channel_Four_Television_Corporation",
                _capital: "£960M",
                _owners: ["UK Gov"]
            },
            "Guardian Media Group": {
                _wikifr: "https://fr.wikipedia.org/wiki/Groupe_Guardian_Media",
                _wikien: "https://en.wikipedia.org/wiki/Guardian_Media_Group",
                _capital: "£1.01B",
                _owners: ["Scott Trust Ltd."]
            },

        /* === NORTH AMERICA === */
            /* --- USA --- */
            "AT&T": {
                _wikifr: "https://fr.wikipedia.org/wiki/AT%26T",
                _wikien: "https://en.wikipedia.org/wiki/AT%26T",
                _capital: "$531B",
                _owners: [""]
            },
            "Walt Disney": {
                _wikifr: "https://fr.wikipedia.org/wiki/The_Walt_Disney_Company",
                _wikien: "https://en.wikipedia.org/wiki/The_Walt_Disney_Company",
                _capital: "$98,6B",
                _owners: [""]
            },
            "Comcast": {
                _wikifr: "https://fr.wikipedia.org/wiki/Comcast",
                _wikien: "https://en.wikipedia.org/wiki/Comcast",
                _capital: "$252B",
                _owners: ["Brian L. Roberts"]
            },
            "Vox Media": {
                _wikifr: "https://fr.wikipedia.org/wiki/Vox_Media",
                _wikien: "https://en.wikipedia.org/wiki/Vox_Media",
                _capital: ">1.1B€",
                _owners: []
            },

            /* --- Canada --- */
            "CBC": {
                _wikifr: "https://fr.wikipedia.org/wiki/Soci%C3%A9t%C3%A9_Radio-Canada",
                _wikien: "https://en.wikipedia.org/wiki/Canadian_Broadcasting_Corporation",
                _capital: "$1.78B",
                _owners: ["Government of Canada"]
            },

        /* === ASIA === */
            /* --- China --- */
            "Media of China": {
                _wikifr: "",
                _wikien: "https://en.wikipedia.org/wiki/Media_of_China",
                _capital: "$?M",
                _owners: ["PCR"]
            },

            /* --- Qatar --- */
            "Al Jazeera": {
                _wikifr: "https://fr.wikipedia.org/wiki/Al_Jazeera_Media_Network",
                _wikien: "https://en.wikipedia.org/wiki/Al_Jazeera_Media_Network",
                _capital: "$?M",
                _owners: ["Qatar"]
            },

            /* --- Russia --- */
            "Rossiya Segodnya": {
                _wikifr: "https://fr.wikipedia.org/wiki/Rossia_Segodnia",
                _wikien: "https://en.wikipedia.org/wiki/Rossiya_Segodnya",
                _capital: "$420M",
                _owners: ["Russia Government"]
            },

        /* === AFRICA === */
            "EuronewsNBC": {
                _wikifr: "",
                _wikien: "",
                _capital: "$77M",
                _owners: ["Naguib Sawiris"]
            },

    }

    // media ---> top corporation
    let media = {
        /* === EU === */
        /* --- FRANCE --- */
            // Le 1%
            "BFMTV": "Altice",
            "RMC": "Altice",
            "i24NEWS": "Altice",
            "i24NEWS Français": "Altice",
            "i24NEWS English": "Altice",
            "i24NEWS Arabic": "Altice",
            "l'express": "Altice",

            "LeHuffPost": "Groupe Le Monde",
            "Le Monde": "Groupe Le Monde",
            "L'Obs": "Groupe Le Monde",

            "Toute l'Histoire": "Mediawan",

            "RTL - On a tellement de choses à se dire": "Bertelsmann",

            "Sud Radio": "Fiducial",

            "LCI": "Bouygues",
            "TF1": "Bouygues",
            "C'est Canteloup": "Bouygues",
            "Palmashow": "Bouygues",
            "Quotidien": "Bouygues",

            "CANAL+": "Bolloré",
            "CNEWS": "Bolloré",
            "C8": "Bolloré",
            "Touche pas à mon poste !": "Bolloré",
            "Canal Plus": "Bolloré",
            "Clique TV": "Bolloré",

            "Figaro": "Groupe Dassault",
            "Figaro Live": "Groupe Dassault",

            "Le Parisien": "LVMH",
            "Les Echos": "LVMH",

            "Europe 1": "Lagardère",

            "africanews (en français)": "EuronewsNBC",
            "euronews (en français)": "EuronewsNBC",

            // governements
            "On n'est pas couché": "France Télévisions",
            "La nuit nous appartient": "France Télévisions",
            "Les Carnets de Julie": "France Télévisions",
            "Ben on the road": "France Télévisions",
            "Le monde de Lisa": "France Télévisions",
            "Deconne Cheese": "France Télévisions",
            "Code Promo": "France Télévisions",
            "L’instant détox": "France Télévisions",
            "Panique dans l'oreillette": "France Télévisions",
            "On n'demande qu'à en rire": "France Télévisions",
            "Draw my news": "France Télévisions",
            "On a tout essayé": "France Télévisions",
            "L’Emission politique": "France Télévisions",
            "Élysée": "France Télévisions",
            "FRANCE 24": "France Télévisions",
            "FRANCE 24 English": "France Télévisions",
            "C Pol": "France Télévisions",
            "C Politique": "France Télévisions",
            "C l'hebdo": "France Télévisions",
            "C à vous": "France Télévisions",
            "Envoyé Spécial": "France Télévisions",
            "TV5MONDE": "France Télévisions",
            "TV5MONDE Info": "France Télévisions",
            "franceinfo": "France Télévisions",
            "France 3 Hauts-de-France": "France Télévisions",
            "France 3 Bretagne": "France Télévisions",
            "Télé Matin": "France Télévisions",
            "C'est pas sorcier": "France Télévisions",

            "France Inter": "Radio France",
            "France Culture": "Radio France",
            "Public Sénat": "La Chaîne parlementaire",
            "Ina Clash TV": "INA",
            "Ina Talk Shows": "INA",
            "Ina Politique": "INA",
            "Ina Histoire": "INA",
            "RFI": "France Médias Monde",
            "AFP": "AFP",
            "AFP news agency": "AFP",

            "ARTE": "Arte",
            "ARTE Radio": "Arte",
            "Tout est vrai (ou presque) - ARTE": "Arte",
            "Blow Up, l'actualité du cinéma (ou presque) - ARTE": "Arte",
            "FUTUREMAG - ARTE": "Arte",
            "Le Dessous des Cartes - ARTE": "Arte",
            "ARTE Creative": "Arte",
            "ARTE Concert": "Arte",
            "Arte TRACKS": "Arte",
            "La Minute Vieille - ARTE": "Arte",
            "28 minutes - ARTE": "Arte",
            "ARTE360": "Arte",
            "ARTE Documentary": "Arte",
            "Vox Pop - ARTE": "Arte",
            "Karambolage en français - ARTE": "Arte",
            "ARTE Cinema": "Arte",
            "Programmes Courts - ARTE": "Arte",
            "TUTOTAL - ARTE": "Arte",
            "Les bandes-annonces d'ARTE": "Arte",
            "BiTS, magazine presque culte - ARTE": "Arte",
            "Tous les internets — ARTE": "Arte",
            "ARTE Junior - en français": "Arte",
            "Tu mourras moins bête - ARTE": "Arte",
            "Programmes Courts - ARTE": "Arte",
            "ARTE Séries": "Arte",
            "ARTE Découverte": "Arte",
            "BiTS auf Deutsch - ARTE": "Arte",
            "FUTUREMAG auf Deutsch - ARTE": "Arte",
            "Karambolage auf Deutsch - ARTE": "Arte",
            "Die Trailer von ARTE": "Arte",
            "Irgendwas mit ARTE und Kultur": "Arte",
            "ARTE Junior - auf Deutsch": "Arte",
            "ARTEde": "Arte",
            "ARTE Serien & Filme": "Arte",
            "VoxPop_ArteDE": "Arte",
            "Alle Internetze - ARTE": "Arte",

            // Associative/Misc
            "Le Média": "Le Média",
            "Mediapart": "Mediapart",
            "madmoiZelle": "Alj Agency",
            "Queen Camille - madmoiZelle": "Alj Agency",
            "Charlie - madmoiZelle": "Alj Agency",
            "The Boys Club": "Alj Agency",
            "Coucou le Q": "Alj Agency",
            "Fabrice FLORENT - Histoires de Darons": "Alj Agency",
            "madmoiZelle music": "Alj Agency",
            "Laisse-moi Kiffer": "Alj Agency",
            "Rockie": "Alj Agency",
            "Mymy - madmoiZelle": "Alj Agency",
            "CONQUÉRANTES": "Alj Agency",
            "Sois gentille, dis merci, fais un bisou": "Alj Agency",
            "Élise Francisse": "Alj Agency",

        /* --- UK --- */
            "Channel 4 News": "Channel 4",
            "Channel 4": "Channel 4",
            "E4": "Channel 4",
            "All 4": "Channel 4",
            "Channel 4 Comedy": "Channel 4",
            "mashed": "Channel 4",
            "Hollyoaks": "Channel 4",
            "4Music": "Channel 4",
            "Made in Chelsea": "Channel 4",
            "Film4": "Channel 4",
            "Random Acts": "Channel 4",

            "BBC News": "BBC",
            "BBC News - Русская служба": "BBC",
            "BBC News عربي": "BBC",
            "BBC Ideas": "BBC",
            "BBC News Türkçe": "BBC",
            "BBC Pashto": "BBC",
            "BBC News اردو": "BBC",
            "BBC Newsnight": "BBC",
            "BBC Click": "BBC",
            "BBC News Tiếng Việt": "BBC",
            "BBC Newsbeat": "BBC",
            "BBC Reel": "BBC",
            "BBC Earth Lab": "BBC",
            "BBC Earth Unplugged": "BBC",
            "Doctor Who": "BBC",
            "BBC Earth": "BBC",
            "BBC": "BBC",
            "BBC Brit": "BBC",
            "BBC Stories": "BBC",
            "BBC Studios": "BBC",
            "Top Gear": "BBC",
            "EastEnders": "BBC",
            "BBC Documentaries": "BBC",
            "BBC Comedy Greats": "BBC",
            "BBC America": "BBC",
            "Anglophenia": "BBC",

            "The Guardian": "Guardian Media Group",
            "Guardian News": "Guardian Media Group",
            "Owen Jones": "Guardian Media Group",
            "Guardian Football": "Guardian Media Group",
            "Guardian Culture": "Guardian Media Group",
            "Guardian Sport": "Guardian Media Group",
            "Guardian Supporters": "Guardian Media Group",
            "Guardian Jobs": "Guardian Media Group",

        /* === NORTH AMERICA === */
            /* --- USA --- */
            "Vox": "Vox Media",
            "CNN": "AT&T",
            "CNNLivestreams": "AT&T",
            "CNN Politics": "AT&T",
            "CNNHeroesYT": "AT&T",
            "HLN": "AT&T",
            "The Daily Share": "AT&T",
            "Adult Swim": "AT&T",
            "TBS": "AT&T",
            "TNT": "AT&T",
            "Full Frontal with Samantha Bee": "AT&T",
            "Team Coco": "AT&T",
            "Cartoon Network": "AT&T",
            "Funny Or Die": "AT&T",

            "Fox News": "Walt Disney",
            "Fox Business": "Walt Disney",
            "Fox News Insider": "Walt Disney",
            "Fox Nation": "Walt Disney",
            "FOX News Radio": "Walt Disney",
            "FOX411": "Walt Disney",
            "Walt Disney Studios": "Walt Disney",
            "DisneyMusicVEVO": "Walt Disney",
            "Disney Music": "Walt Disney",
            "Disney Channel": "Walt Disney",
            "Disney": "Walt Disney",
            "Star Wars": "Walt Disney",
            "disneymusicasiaVEVO": "Walt Disney",
            "DisneyPolskaVEVO": "Walt Disney",

            "Sky News": "Comcast",
            "The Pledge": "Comcast",
            "Sky Sports": "Comcast",
            "Sky Cinema": "Comcast",
            "Sky One": "Comcast",
            "Sky Arts": "Comcast",
            "Sky Witness": "Comcast",
            "Sky Atlantic": "Comcast",
            "Sky Sports Boxing": "Comcast",
            "Sky Sports Football": "Comcast",
            "Sky Sports F1": "Comcast",

            /* --- Canada --- */
            "CBC News": "CBC",
            "CBC": "CBC",
            "CBC Sports": "CBC",
            "CBC Kids News": "CBC",
            "CBC Arts": "CBC",
            "Radio-Canada": "CBC",
            "The Fifth Estate": "CBC",
            "CBC News: The National": "CBC",
            "CBC Nova Scotia": "CBC",
            "CBC NL - Newfoundland and Labrador": "CBC",
            "CBC Music": "CBC",
            "CBC Music Lab": "CBC",
            "q on cbc": "CBC",
            "Strombo": "CBC",
            "Dragon's Den": "CBC",
            "MercerReport": "CBC",
            "Joint Taskforce-Atlantic": "CBC",
            "CBC Vancouver": "CBC",


        /* === Russia === */
        "Sputnik France": "Rossiya Segodnya",
        "SputnikFrance": "Rossiya Segodnya",
        "RT": "Rossiya Segodnya",
        "RT France": "Rossiya Segodnya",
        "RT Arabic": "Rossiya Segodnya",
        "RT Online": "Rossiya Segodnya",
        "RT America": "Rossiya Segodnya",
        "RT Chinese": "Rossiya Segodnya",
        "RT en Español": "Rossiya Segodnya",
        "Ruptly": "Rossiya Segodnya",
        "Raw Take": "Rossiya Segodnya",
        "COMMANDOS": "Rossiya Segodnya",
        "breakingtheset": "Rossiya Segodnya",
        "Watching the Hawks RT": "Rossiya Segodnya",
        "TheAlyonaShow": "Rossiya Segodnya",
        "CapitalAccount": "Rossiya Segodnya",
        "RT на русском": "Rossiya Segodnya",
        "The Big Picture RT": "Rossiya Segodnya",
        "Redacted Tonight": "Rossiya Segodnya",
        "LearnRussianwithRT": "Rossiya Segodnya",
        "RT UK": "Rossiya Segodnya",
        "RT Deutsch": "Rossiya Segodnya",
        "Der Fehlende Part": "Rossiya Segodnya",
        "451 Grad": "Rossiya Segodnya",
        "RT Documentary": "Rossiya Segodnya",
        "Boom Bust": "Rossiya Segodnya",
        "primetimeru": "Rossiya Segodnya",
        "WorldsApaRT": "Rossiya Segodnya",
        "RT Sport": "Rossiya Segodnya",

        "RTVI": "Gazprom",

        /* === China === */
        "People's Daily, China 人民日报": "Media of China",
        "New China TV": "Media of China",
        "Tân Hoa": "Media of China",
        "China Xinhua Español": "Media of China",
        "Xinhua Myanmar": "Media of China",
        "CCTV中国中央电视台": "Media of China",
        "中国音乐电视": "Media of China",
        "CCTV今日说法官方频道": "Media of China",
        "CCTV综艺": "Media of China",
        "CCTV军事": "Media of China",
        "CCTV少儿": "Media of China",
        "CCTV农业": "Media of China",
        "中华国宝": "Media of China",
        "CCTV春晚": "Media of China",
        "CCTV挑战不可能官方频道": "Media of China",
        "CCTV戏曲": "Media of China",
    }

    /*document.querySelectorAll("ytd-channel-renderer").forEach( el => {
    })*/

    document.querySelectorAll("ytd-video-renderer").forEach( el => {
        let name = el.querySelector("yt-formatted-string").getAttribute('title')
        if (media.hasOwnProperty(name)) {
            let corp = media[name]

            let capital = corporations[corp]["_capital"]
            let wiki = corporations[corp]["_wikifr"]
            let owner = ""
            let fortune = ""
            if (corporations[corp]["_owners"].length > 0) {
                owner = corporations[corp]["_owners"][0]
                fortune = owners[owner["fortune"]]
            }

            let chan = el.querySelector("yt-formatted-string").firstChild
            let html = "<b>[<a href='" + wiki + "'>🇫🇷" + corp.toUpperCase() + " (" + capital + ") </a>- <a href='"
                + owners[owner["wikifr"]] + "'>" + owner.toUpperCase() + "</a> "
                + name + " (" + fortune + ")]</b>"
            chan.innerHTML = html
            chan.setAttribute('style', 'color: red; font-size: 1.8rem')
            el.setAttribute('style', 'background-color: #edc9fc;')
        } else {
            console.debug( "unlisted: " + name )
            el.setAttribute('style', 'background-color: rgba(0, 0, 0, 0);')
            el.querySelector("yt-formatted-string").firstChild.setAttribute('style', 'color: rgb(96, 96, 96); font-size: 13px;')
        }
    })

    setTimeout(disown, 2000)
}())
